# LMS frontend site

**Features**

- ⚛️ React 18
- 🌊 Custom Document and App
- ⌨️ Type safety using TypeScript in strict mode

## Requirements

- `Node.js` v16.16.0 LTS or later
- `yarn`

## Quick start

- `yarn install`
- `yarn start`

